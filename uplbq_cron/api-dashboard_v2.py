import pandas as pd
import numpy as np
import pymysql
def get_conn_sys():
    return pymysql.connect(host='34.87.78.49',
                   database='upteamco',
                   user='upteam-gcp-readonly',
                   password='hjryvnJH873&^1hdfsj9856')

query = """
SELECT
	CONVERT_TZ(NOW(),'UTC','+08:00') as insert_date
    ,p.id as product_id
	,p.sku
	,pur.id as purchase_id
	,pur.split_from
	,COALESCE(pur.split_from,pur.id) final_purchase_id
	,pur.reference
	,CASE
		WHEN pur.reference like '%Auction Brandear Virtual%' then 1 else 0 end api_integration
	,IF(DATE(p.published_date) = '0000-00-00', Null,
		DATE(p.published_date)) as published_date
	,p.acquire_date
	,p.status
	,p.selling_price_usd as base_selling_price
	,p.cost_usd as prod_cost_usd
	,sup.name
	,sup.shortcode
	# Product Fields
	,b.name as product_brand
	,cat.parent_category as product_parent_category
	,cat.name as product_category
	,p.name as product_name
	,p.rating AS product_rating -- new column by customer reseg
    ,CONCAT(tx.brand,"-",tx.category,"-",tx.style,"-",tx.code,"-",tx.material,"-",tx.color,"-",tx.size) AS index_name
FROM upteamco.products p
	INNER JOIN upteamco.purchases pur ON p.purchase_id = pur.id
	INNER JOIN upteamco.suppliers sup ON pur.supplier_id = sup.id
	INNER JOIN upteamco.brands b ON p.brand = b.id
	INNER JOIN upteamco.categories cat ON p.category = cat.id
    INNER JOIN product_meta mt on mt.product = p.id and mt.key = 'index_id'
    LEFT JOIN upteam_model_info.taxonomy_index tx ON tx.index_id = mt.value
WHERE 1=1
	AND sup.id = 83
ORDER BY acquire_date DESC
"""
df = pd.read_sql(query,get_conn_sys())

query_2 = """
select
	CONVERT_TZ(NOW(),'UTC','+08:00') as insert_date
    ,c.id as customer_id
	,c.customer_name
    ,c.business_type as customer_type
	,pm.coverage
	,p.name  as product_name
	,p.sku as product_sku
	,pm.price
	,pm.insert_date AS assigned
from product_marketplace  pm
INNER JOIN products p  ON pm.product_id = p.id
INNER JOIN customers c ON c.id = pm.customer_id
INNER JOIN (SELECT
				p.sku
			FROM upteamco.products p
				INNER JOIN upteamco.purchases pur ON p.purchase_id = pur.id
				INNER JOIN upteamco.suppliers sup ON pur.supplier_id = sup.id
				INNER JOIN upteamco.brands b ON p.brand = b.id
				INNER JOIN upteamco.categories cat ON p.category = cat.id
			WHERE 1=1
				AND sup.id = 83
				AND CASE WHEN pur.reference like '%Auction Brandear Virtual%' then 1 else 0 end = 1) a on a.sku = p.sku
WHERE 1=1
AND  pm.reason_removed = 'NA'
"""
df_2 = pd.read_sql(query_2,get_conn_sys())

from google.oauth2 import service_account
import pandas_gbq

credentials = service_account.Credentials.from_service_account_info(
    {
  "type": "service_account",
  "project_id": "upteam-data-projects",
  "private_key_id": "c25b3189d55e5fdbb6624482f69b46ae8e359f3b",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDQ+EcC4wOdDsCf\nbyZpcPmqa64/ldHx59egIomT0Ab6yF8Siev+vQzdSRjSOC9q5vlgvW+u3gWaIcdm\nrn376svPLey+Od8Rma7+PX+8zqn5QDHLOKqKeu8jGwVenlUx5mAhCbXAIoswUhrl\nGdpMJGkNrJB9VlfNTZ1staEUic92mzPPRMMce+m9t1agjWZ/4DudtQddquUF4NXN\nuQZpdt1mY0OXswB252qmhiJ8pd36fX3nmTBvJs7q1TTlfQ18MW4B8sWL32WUqDHy\n3wAp9X6ag9Tl7B2p8O/WSzPY8NVSsyYRDrl/VZ0OkiJOogYJb5LDexMBJbMwYctG\n2Qc4LA3xAgMBAAECggEAFtFFT7diCaA2wDHuy8yeNlGTsTW2uudJ0B51T0KDwJZo\naynMLwCNiwwUPcAMzWtzB3En5LwxeGb/0uJytzMoFfM5ynufM9jIFtK4Q/F89c5g\noYQKexx5SCgHKhG3078H+BOLWPNsp1YPwbxU7cN8BRY2oLjw1WYILnqWoBt8H8MY\nGrTwpuM/lu/jYJSlt3LUUyJe5c78k16b2XaEo1rrr8AmFySUyNx3voZUPy5G0B7a\nb/7pbqeL9j3gnqgzijHcDH5769zos5qCFb6n4mf7rs75sILVA8VfOnyfy64jNo02\nMz6HKKohgFQAmtha0m/xlYWUyQG3WlaBzrY0pnfYkQKBgQDs67ZHA8oIV+xxs0c9\nKssFmNnJsgpmQgSGRxz/CAOL7GkHsTReoBCq31ZGqa53fKq/E8PC91XtsaEYPugl\ni7BFL6mU51L9QLN6mbF77kwe4c1GLq8O46yYFNp1SvxC13phr9BkqAAe3CBURwdD\nCTUB09lWIUl7ZAsawK8JcLT5OwKBgQDhzFZp7emZfCJi+nAmUuq4Kq48YEpopNZ4\nvC+hJIHOKUbeMobKkfbsmndzc9wkAkVq3vJs1vCyXenirWvCOCVarZACDWic+B25\nfFpj/5DUY3m5O97Ycpz0mDvcKiXWSNYlhrVx6y2idKUAAv25mYeNKcJIA9TDmguO\n/u/quSNCwwKBgQDFNY+MnULWnxlZq3lpGO3Vwt3GXkT5yYoqm5cvUmRhXHAwzvhg\nq6w0D6OpPiTToRof4zvYVjlYmbsWi4aBej1H2dy27bbOx8SeEMED/9+s9RgBi2p8\nYleQYAtYBwrkOKjOF6LHpnAzP9hJTrZkvo048DbA0wRviV8U7b7FhOOfcQKBgQCt\nh1pJ4nuKZ2oN4yXZbhPRIYvx/pZwwW3KL+Xy05mmlPZPwUjx2SeJQlTWt43RZqVu\nspRgrPciPSaH/Hs6MegTPhvUV/Wz3MMLT9Qv0bK+ZwyfHLHwanTRTUWDuCiH/EYH\nZPWA7RqPJVHBJotT3ffctuEGc1tKT9LePHKpU0sT/QKBgHrTs6wEZgDBTG5WDdDD\nfK9ggWT9jGvw2ny4xrIcpRpucYOz88oqmxm3aBtXW2wnOr2dD+/1G4uor7it3qwh\nwV8LmGcSozDxDwyuTqyQ/nxLDwHfQusNTm1yYnI0DTXked5DLLz2c9nNCWszxs4G\n92LCU4WD6e2lr0gDCSKCJf27\n-----END PRIVATE KEY-----\n",
  "client_email": "local-python-bigquery-write@upteam-data-projects.iam.gserviceaccount.com",
  "client_id": "105996479783271550789",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/local-python-bigquery-write%40upteam-data-projects.iam.gserviceaccount.com"
    },)

df.to_gbq(destination_table='chrono_jobs.api_dashboard',
          project_id='upteam-data-projects',
          table_schema =  [
                            {'name':'insert_date', 'type':'DATETIME'},
                            {'name':'id', 'type':'INTEGER'},
                            {'name':'sku', 'type':'STRING'},
                            {'name':'id', 'type':'INTEGER'},
                            {'name':'split_from', 'type':'INTEGER'},
                            {'name':'final_id', 'type':'INTEGER'},
                            {'name':'reference', 'type':'STRING'},
                            {'name':'api_integration', 'type':'INTEGER'},
                            {'name':'published_date', 'type':'DATETIME'},
                            {'name':'acquire_date', 'type':'DATETIME'},
                            {'name':'status', 'type':'STRING'},
                            {'name':'base_selling_price', 'type':'FLOAT'},
                            {'name':'prod_cost_usd', 'type':'FLOAT'},
                            {'name':'name', 'type':'STRING'},
                            {'name':'shortcode', 'type':'STRING'},
                            {'name':'product_brand', 'type':'STRING'},
                            {'name':'product_parent_category', 'type':'STRING'},
                            {'name':'product_category', 'type':'STRING'},
                            {'name':'product_name', 'type':'STRING'},
                            {'name':'product_rating', 'type':'STRING'},
                          ],
          if_exists='replace', credentials=credentials)
print(df.shape)

df_2.to_gbq(destination_table='chrono_jobs.api_allocate_products_dashboard',
          project_id='upteam-data-projects',
          table_schema =  [
                            {'name':'insert_date', 'type':'DATETIME'},
                            {'name':'customer_id', 'type':'INTEGER'},
                            {'name':'customer_name', 'type':'STRING'},
                            {'name':'customer_type', 'type':'STRING'},
                            {'name':'coverage', 'type':'STRING'},
                            {'name':'product_name', 'type':'STRING'},
                            {'name':'product_sku', 'type':'STRING'},
                            {'name':'price', 'type':'FLOAT'},
                            {'name':'assigned', 'type':'DATETIME'},
                          ],
          if_exists='replace', credentials=credentials)
print(df2.shape)
print("Update Completed")
