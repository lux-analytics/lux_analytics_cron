#!/usr/bin/env python
# coding: utf-8

# # This script is used to query the Sales Table from Legacy to Bigquery
#
# ## Before running this script, backup the present data

# In[1]:


import pandas as pd
import numpy as np
import pymysql
import datetime as dt
from datetime import timedelta


# In[2]:


from google.oauth2 import service_account
import pandas_gbq


credentials = service_account.Credentials.from_service_account_info(
    {
  "type": "service_account",
  "project_id": "upteam-data-projects",
  "private_key_id": "c25b3189d55e5fdbb6624482f69b46ae8e359f3b",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDQ+EcC4wOdDsCf\nbyZpcPmqa64/ldHx59egIomT0Ab6yF8Siev+vQzdSRjSOC9q5vlgvW+u3gWaIcdm\nrn376svPLey+Od8Rma7+PX+8zqn5QDHLOKqKeu8jGwVenlUx5mAhCbXAIoswUhrl\nGdpMJGkNrJB9VlfNTZ1staEUic92mzPPRMMce+m9t1agjWZ/4DudtQddquUF4NXN\nuQZpdt1mY0OXswB252qmhiJ8pd36fX3nmTBvJs7q1TTlfQ18MW4B8sWL32WUqDHy\n3wAp9X6ag9Tl7B2p8O/WSzPY8NVSsyYRDrl/VZ0OkiJOogYJb5LDexMBJbMwYctG\n2Qc4LA3xAgMBAAECggEAFtFFT7diCaA2wDHuy8yeNlGTsTW2uudJ0B51T0KDwJZo\naynMLwCNiwwUPcAMzWtzB3En5LwxeGb/0uJytzMoFfM5ynufM9jIFtK4Q/F89c5g\noYQKexx5SCgHKhG3078H+BOLWPNsp1YPwbxU7cN8BRY2oLjw1WYILnqWoBt8H8MY\nGrTwpuM/lu/jYJSlt3LUUyJe5c78k16b2XaEo1rrr8AmFySUyNx3voZUPy5G0B7a\nb/7pbqeL9j3gnqgzijHcDH5769zos5qCFb6n4mf7rs75sILVA8VfOnyfy64jNo02\nMz6HKKohgFQAmtha0m/xlYWUyQG3WlaBzrY0pnfYkQKBgQDs67ZHA8oIV+xxs0c9\nKssFmNnJsgpmQgSGRxz/CAOL7GkHsTReoBCq31ZGqa53fKq/E8PC91XtsaEYPugl\ni7BFL6mU51L9QLN6mbF77kwe4c1GLq8O46yYFNp1SvxC13phr9BkqAAe3CBURwdD\nCTUB09lWIUl7ZAsawK8JcLT5OwKBgQDhzFZp7emZfCJi+nAmUuq4Kq48YEpopNZ4\nvC+hJIHOKUbeMobKkfbsmndzc9wkAkVq3vJs1vCyXenirWvCOCVarZACDWic+B25\nfFpj/5DUY3m5O97Ycpz0mDvcKiXWSNYlhrVx6y2idKUAAv25mYeNKcJIA9TDmguO\n/u/quSNCwwKBgQDFNY+MnULWnxlZq3lpGO3Vwt3GXkT5yYoqm5cvUmRhXHAwzvhg\nq6w0D6OpPiTToRof4zvYVjlYmbsWi4aBej1H2dy27bbOx8SeEMED/9+s9RgBi2p8\nYleQYAtYBwrkOKjOF6LHpnAzP9hJTrZkvo048DbA0wRviV8U7b7FhOOfcQKBgQCt\nh1pJ4nuKZ2oN4yXZbhPRIYvx/pZwwW3KL+Xy05mmlPZPwUjx2SeJQlTWt43RZqVu\nspRgrPciPSaH/Hs6MegTPhvUV/Wz3MMLT9Qv0bK+ZwyfHLHwanTRTUWDuCiH/EYH\nZPWA7RqPJVHBJotT3ffctuEGc1tKT9LePHKpU0sT/QKBgHrTs6wEZgDBTG5WDdDD\nfK9ggWT9jGvw2ny4xrIcpRpucYOz88oqmxm3aBtXW2wnOr2dD+/1G4uor7it3qwh\nwV8LmGcSozDxDwyuTqyQ/nxLDwHfQusNTm1yYnI0DTXked5DLLz2c9nNCWszxs4G\n92LCU4WD6e2lr0gDCSKCJf27\n-----END PRIVATE KEY-----\n",
  "client_email": "local-python-bigquery-write@upteam-data-projects.iam.gserviceaccount.com",
  "client_id": "105996479783271550789",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/local-python-bigquery-write%40upteam-data-projects.iam.gserviceaccount.com"
    },)


# In[3]:


query = """
SELECT *
FROM
dashboard.Sales
"""
backup_sales = pd.read_gbq(query, project_id='upteam-data-projects',credentials=credentials)


day = ((dt.datetime.today()).date()).strftime('%Y-%m-%d')
backup_sales.to_csv('sales_backup'+day+'.csv', index=False)


user = 'upteam-gcp-readonly'
password = 'hjryvnJH873&^1hdfsj9856'
host = '34.87.78.49'
database = 'upteamco'

db = pymysql.connect(user=user,password=password,host=host,database=database)


# In[7]:


NOW = dt.datetime.today().date().strftime('%Y%m%d')
yesterday = ((dt.datetime.today() - timedelta(days=1)).date()).strftime('%Y-%m-%d')



# In[8]:


rerun = '''

SELECT
  CONVERT_TZ(NOW(),'UTC','+08:00') as insert_date,
  tmp.*

FROM
  (SELECT
    isl.first_dispatch_date,
    i.id as invoice_id,
    p.sku,
    i.shipping_date,
    su.type as supplier_type,
    su.name as supplier_name,
    su.shortcode as supplier_shortcode,
    su.country as supplier_country, -- new by client resegment
    DATE(p.published_date) AS published_date,
    selling_price_usd AS 'selling_price_usd',
    IFNULL(
      cpod.base_selling_usd_original,
      p.selling_price_usd
    ) AS 'base_price',
    ROUND(
      (
        (
          (r.shipping_multiplier - 1) * IFNULL(
            cpod.base_selling_usd_original,
            p.selling_price_usd
          )
        ) + r.shipping_addon + IFNULL(p.addon_usd_logistics, 0)
      ),
      2
    ) AS 'logistics_rev_usd',
    ROUND(
      (
        (
          (r.customs_multiplier - 1) * IFNULL(
            cpod.base_selling_usd_original,
            p.selling_price_usd
          )
        ) + r.customs_addon
      ),
      2
    ) AS 'duties_rev_usd',
    ROUND(
      IF(
        cpod.final_customer_price = 0,
        cpod.final_price,
        final_customer_price
      ) * (
        (1 / (1 + (cpo.inclusive_vat_rate / 100))) * (cpo.inclusive_vat_rate / 100)
      ) * IF(sold > 0, sold, 0),
      2
    ) AS 'vat_lost_usd',
    p.no_tax AS 'no_tax',
    p.with_tax AS 'with_tax',
    cost_usd AS 'product_cost_usd',
    pr.price AS 'reservation_price_usd',
    cpod.final_price AS 'final_price_usd',
    ROUND(
      IF(
        cpod.final_customer_price = 0,
        cpod.final_price,
        final_customer_price
      ),
      2
    ) AS 'shopper_price_usd',
    r.currency AS display_currency,
    IF(
      r.currency = 'USD',
      final_price,
      foreign_price
    ) AS foreign_cpo_price,
    r.billing_currency,
    ROUND(
      IF(
        r.billing_currency = r.currency,
        IF(
          r.currency = 'USD',
          final_price,
          foreign_price
        ),
        final_price * billing_conversion
      ),
      2
    ) AS billing_invoice_price,

    CASE
        WHEN c.customer_name IN ('FFE','FFENV') THEN 'FFE'
        ELSE c.customer_name
    END as customer_name,


    c.customer_shortcode,
    cpo.customer_reference,
    c.business_type,
    TRIM(
      SUBSTRING_INDEX(c.business_type, '-', 1)
    ) AS business,
    c.channel, -- new by client resegment
    c.cluster, -- new by client resegment
    c.country as customer_country, -- new by client resegment

    c.upteam_contact,
    DATE(cpo.makedate) AS po_date,
    DATE(opendate) AS create_date,
    i.invoice_no,
    i.status AS invoice_status,
    p.current_location,
    p.status AS product_status,
    r.campaign_reference AS reservation_reference,
    DATE(i.makedate) AS invoice_date,
    i.carrier_type,
    i.tracking_number,


    # Product Fields
    b.name as product_brand,
    cat.parent_category as product_parent_category,
    cat.name as product_category,
    p.name as product_name,
    p.rating as product_rating, -- new by client resegment

    # Taxonomy Index Fields
    tx.brand as idx_brand,
    tx.category as idx_category,
    tx.`style` as idx_style,
    tx.`code` as idx_code,
    tx.`color` as idx_color,
    tx.`material` as idx_material,
    tx.`size` as idx_size,
    mt2.`value` as product_tags,
    NULL as profit_share


  FROM
    customer_purchase_orders cpo
    INNER JOIN customer_purchase_order_details cpod
      ON cpod.customer_po_id = cpo.id
    INNER JOIN product_reservations pr
      ON pr.id = cpod.reservation_detail_id
    INNER JOIN products p
      ON p.id = pr.product_id
    INNER JOIN customers c
      ON c.id = cpo.customer_id
    INNER JOIN reservations r
      ON r.id = pr.reservation_id
    LEFT JOIN invoice_details id
      ON id.customer_po_id = cpo.id
    LEFT JOIN invoices i
      ON i.id = id.invoice_id
    INNER JOIN purchases pu
      ON pu.id = p.purchase_id
    INNER JOIN suppliers su
      ON su.id = pu.supplier_id

    INNER JOIN upteamco.brands b
      ON p.brand = b.id
    INNER JOIN upteamco.categories cat
      ON p.category = cat.id
    LEFT JOIN upteamco.product_meta mt
      ON mt.product = p.id AND mt.key = 'index_id'
    LEFT JOIN upteam_model_info.`taxonomy_index` tx
      ON tx.`index_id` = mt.`value`

    LEFT JOIN product_meta mt2
      ON p.id = mt2.product AND mt2.key = 'product_tags'

    LEFT JOIN
      (SELECT invoice_id, MIN(date_updated) as first_dispatch_date
        FROM invoice_status_logs isl
        WHERE isl.new_status in ('dispatched','sent')
        GROUP BY isl.invoice_id
      ) as isl ON isl.invoice_id = i.id

  WHERE 1 = 1
    AND DATE(i.makedate) >= '2018-01-01'
    AND DATE(i.makedate) <= '{}'
    AND i.id IS NOT NULL
    AND i.status in ('dispatched', 'sent', 'delivered', 'completed')

  ORDER BY cpo.makedate) tmp

'''

df = pd.read_sql(rerun.format(yesterday), db)

df_no_duplicates = df.drop_duplicates(subset=['sku','invoice_id'],keep='last')



print(backup_sales.shape)
print(df.shape)
print(df_no_duplicates.shape)
df_no_duplicates.to_gbq(destination_table='dashboard.Sales',
          project_id='upteam-data-projects',
          table_schema =  [
                            {'name':'insert_date', 'type':'DATETIME'},
                            {'name':'first_dispatch_date', 'type':'DATETIME'},
                            {'name':'invoice_id', 'type':'FLOAT'},
                            {'name':'sku', 'type':'STRING'},
                            {'name':'shipping_date', 'type':'DATETIME'},
                            {'name':'supplier_type', 'type':'STRING'},
                            {'name':'supplier_name', 'type':'STRING'},
                            {'name':'supplier_shortcode', 'type':'STRING'},
                            {'name':'published_date', 'type':'DATETIME'},
                            {'name':'selling_price_usd', 'type':'FLOAT'},
                            {'name':'base_price', 'type':'FLOAT'},
                            {'name':'logistics_rev_usd', 'type':'FLOAT'},
                            {'name':'duties_rev_usd', 'type':'FLOAT'},
                            {'name':'vat_lost_usd', 'type':'FLOAT'},
                            {'name':'no_tax', 'type':'FLOAT'},
                            {'name':'with_tax', 'type':'FLOAT'},
                            {'name':'product_cost_usd', 'type':'FLOAT'},
                            {'name':'reservation_price_usd', 'type':'FLOAT'},
                            {'name':'final_price_usd', 'type':'FLOAT'},
                            {'name':'shopper_price_usd', 'type':'FLOAT'},
                            {'name':'display_currency', 'type':'STRING'},
                            {'name':'foreign_cpo_price', 'type':'FLOAT'},
                            {'name':'billing_currency', 'type':'STRING'},
                            {'name':'billing_invoice_price', 'type':'FLOAT'},
                            {'name':'customer_name', 'type':'STRING'},
                            {'name':'customer_shortcode', 'type':'STRING'},
                            {'name':'customer_reference', 'type':'STRING'},
                            {'name':'business_type', 'type':'STRING'},
                            {'name':'upteam_contact', 'type':'STRING'},
                            {'name':'po_date', 'type':'DATETIME'},
                            {'name':'create_date', 'type':'DATETIME'},
                            {'name':'invoice_no', 'type':'STRING'},
                            {'name':'invoice_status', 'type':'STRING'},
                            {'name':'current_location', 'type':'STRING'},
                            {'name':'product_status', 'type':'STRING'},
                            {'name':'business', 'type':'STRING'},
                            {'name':'reservation_reference', 'type':'STRING'},
                            {'name':'invoice_date', 'type':'DATETIME'},
                            {'name':'carrier_type', 'type':'STRING'},
                            {'name':'tracking_number', 'type':'STRING'},
                            {'name':'product_brand', 'type':'STRING'},
                            {'name':'product_parent_category', 'type':'STRING'},
                            {'name':'product_category', 'type':'STRING'},
                            {'name':'product_name', 'type':'STRING'},
                            {'name':'idx_brand', 'type':'STRING'},
                            {'name':'idx_category', 'type':'STRING'},
                            {'name':'idx_style', 'type':'STRING'},

                            {'name':'idx_code', 'type':'STRING'}, # Changed
                            {'name':'idx_color', 'type':'STRING'},
                            {'name':'idx_material', 'type':'STRING'},
                            {'name':'idx_size', 'type':'STRING'},
                            {'name':'product_tags', 'type':'STRING'},
                            {'name':'profit_share', 'type':'FLOAT'},

                            {'name':'customer_country', 'type':'STRING'},
                            {'name':'channel', 'type':'STRING'},
                            {'name':'cluster', 'type':'STRING'},
                            {'name':'supplier_country', 'type':'STRING'},
                            {'name':'product_rating', 'type':'STRING'}
                          ],
          if_exists='replace', credentials=credentials)
print('Dump Successful!')


# In[13]:


import smtplib
import config
from email.message import EmailMessage


# In[14]:


recipients = ['data@luxclusif.com']#['cassy@upteamco.com','migee.ringler@upteamco.com','carmelo@upteamco.com','j.abella@upteamco.com']
#cc = ['daniel.tipan@luxclusif.com']#['r.rico@upteamco.com','veronica.bautista@upteamco.com']


msg = EmailMessage()
msg['Subject'] = 'Weekly Dashboard Rerun'
msg['From'] = 'daniel.tipan@upteamco.com'
msg['To'] = ", ".join(recipients)
#msg['Cc'] = ", ".join(cc)
msg.set_content('''
Hello Analytics Team,

The Dashboard Sales data has been updated. Please check Google Data Studio if there's an error.

Pre-run Data Count: {} invoices
New Data with No Duplicates Count: {} invoices

Regards,
Email - Bot
'''.format(backup_sales.shape[0], df_no_duplicates.shape[0]))



with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:

    smtp.login('daniel.tipan@upteamco.com', 'xkdnkywoufoxsljv')
    smtp.send_message(msg)
    smtp.quit()
